<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require ("animal.php");
        $sheep = new Animal("shaun");
        echo $sheep->name. "<br>"; // "shaun"
        echo $sheep->legs. "<br>"; // 2
        echo $sheep->cold_blooded. "<br>"; // false

        require ("Frog.php");
        require ("Ape.php");
        $sungokong = new Ape("kera sakti");
        $sungokong->yell(); // "Auooo"
        $kodok = new Frog("buduk");
        $kodok->jump(); // "hop hop"
    ?>
</body>
</html>